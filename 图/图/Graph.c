#define _CRT_SECURE_NO_WARNINGS 1
#include"Graph.h"


//////////////////邻接矩阵////////////////

//建立无相网图的邻接矩阵
void CreateMGraph(MGraph* G)
{
	int i, j, k, w;
	printf("输入顶点数和边数\n");
	scanf("%d %d", &G->numNodes, &G->numEdges);
	getchar();
	printf("输入顶点\n");
	for (i = 0; i < G->numNodes; i++)
	{
		scanf("%c", &G->vexs[i]);
		getchar();
	}
	
	//邻接矩阵的初始化
	for (i = 0; i < G->numNodes; i++)
	{
		for (j = 0; j < G->numNodes; j++)
		{
			G->arc[i][j] = INFINITY;
		}
	}

	//给把图表示在邻接矩阵中
	for (k = 0; k < G->numEdges; k++)
	{
		printf("输入边对应的横纵坐标及其权值：横 纵 权值  (下标从0开始)\n");

		scanf("%d %d %d", &i, &j, &w);
		G->arc[i][j] = w;
		G->arc[j][i] = G->arc[i][j];
	}
}

void Print1MGraph(MGraph* G)
{
	int i = 0, j = 0;
	for (i = 0; i < G->numNodes; i++)
	{
		for (j = 0; j < G->numNodes; j++)
		{
			printf("%-5d ", G->arc[i][j]);
		}
		printf("\n");
	}
}


//////////////邻接表//////////////////

void CreateALGraph(GraphAdjList* G)
{
	int i, j, k;
	EdgeNode* e;
	printf("输入顶点数和边数\n");
	scanf("%d %d", &G->numNodes, &G->numEdges);
	getchar();
	printf("请输入所有的结点\n");
	for (i = 0; i < G->numNodes; i++)
	{
		scanf("%c", &G->adjlist[i].data);//输入顶点信息
		getchar();
		G->adjlist[i].firstedge = NULL;//边表置为空
	}

	for (k = 0; k < G->numEdges; k++)//建立边表
	{
		printf("输入边（vi vj）上的顶点序号\n");
		scanf("%d %d", &i, &j);
		//头插法建立单链表（就是顶点的边表）
		//头插法：新结点永远当头
		e = (EdgeNode*)malloc(sizeof(EdgeNode));//生成边表结点
		e->adjvex = j;
		e->next = G->adjlist[i].firstedge;
		G->adjlist[i].firstedge = e;
		e = (EdgeNode*)malloc(sizeof(EdgeNode));//继续生成边表界点
		e->adjvex = i;
		e->next = G->adjlist[j].firstedge;
		G->adjlist[j].firstedge = e;
	}
}


////////邻接矩阵的深度优先递归算法////

void DFS(MGraph G, int i)
{
	int j;
	visited[i] = true;
	printf("%c ", G.vexs[i]);//打印顶点
	for (j = 0; j < G.numNodes; j++)
	{
		if (G.arc[i][j] == 1 && !visited[j])
			DFS(G, j);
	}
}


/////邻接矩阵的深度遍历操作//////////

void DFSTraverse(MGraph G)
{
	int i;
	for (i = 0; i < G.numNodes; i++)
	{
		visited[i] = false;
	}
	for (i = 0; i < G.numNodes; i++)
	{
		if (!visited[i])
		{
			DFS(G, i);
		}
	}
}