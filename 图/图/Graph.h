#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

/////////////////////////邻接矩阵/////////////////////////

typedef char VertexType;//顶点类型
typedef int EdgeType;//边上的权值
#define MAXVEX 100//最大顶点数
#define INFINITY 99999//表示无穷infinity


//邻接矩阵
typedef struct
{
	VertexType vexs[MAXVEX];//顶点表
	EdgeType arc[MAXVEX][MAXVEX];//邻接矩阵,看作边表
	int numNodes;//顶点数
	int numEdges;//边数
}MGraph;

//建立无向网图的邻接矩阵表示
void CreateMGraph(MGraph* G);

//邻接矩阵的打印
void Print1MGraph(MGraph* G);

///////////////////////邻接表////////////////////////////

//邻接表
typedef char VertexType;//顶点类型
typedef int EdgeType;//边上的权值

typedef struct EdgeNode//边表结点
{
	int adjvex;//邻接点域，存储改顶点对应的下标
	EdgeType info;//用于存储权值，对于非网图可以不要
	struct EdgeNode* next;//链域，指向下一个邻接点
}EdgeNode;

typedef struct VertexNode//顶点表结点
{
	VertexType data;//顶点域，存储顶点信息
	EdgeNode* firstedge;//边表头指针
}VertexNode,AdjList[MAXVEX];

typedef struct
{
	AdjList adjlist;
	int numNodes;//图中当前顶点数
	int numEdges;//边数
}GraphAdjList;

//建立无向图的邻接表结构
void CreateALGraph(GraphAdjList* G);


///////////////图的遍历//////////////////

//邻接矩阵的深度优先遍历算法
bool visited[MAXVEX];//初始化内容默认都是false//访问标志的数组
void DFS(MGraph G, int i);

//邻接矩阵的深度遍历操作
void DFSTraverse(MGraph G);

//邻接矩阵的广度优先遍历