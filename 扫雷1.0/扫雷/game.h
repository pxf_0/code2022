#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define AMOUNT 10

//菜单
void menu();

//棋盘初始化
void InitBoard(char board[ROWS][COLS], int rows, int cols, char ch);

//展示棋盘
void Display(char board[ROWS][COLS], int row, int col);

//布置雷
void SetMine(char board[ROWS][COLS], int row, int col);

//选择坐标区域雷的数量
int AmountMine(char board[ROWS][COLS], int row, int col);

//扫雷
void FindMine(char mine[ROWS][COLS],char show[ROWS][COLS], int row, int col);