#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void menu()
{
	printf("************************\n");
	printf("*****    1.play    *****\n");
	printf("*****    0.exit    *****\n");
	printf("************************\n");
}

void InitBoard(char board[ROWS][COLS], int rows, int cols, char ch)
{
	int i = 0, j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = ch;
		}
	}
}

void Display(char board[ROWS][COLS], int row, int col)
{
	printf("-------------------\n");
	int i = 0, j = 0;
	for (j = 0; j <= col; j++)//这个for循环是打印棋盘的列坐标
	{
		printf("%d ", j);
	}
	printf("\n");

	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);//这一行语句是打印棋盘的行坐标
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("-------------------\n");
}

void SetMine(char board[ROWS][COLS], int row, int col)
{
	int x = 0, y = 0;
	int count = AMOUNT;
	while (count)
	{
		x = rand() % row + 1;//(0~8)+1==1~9
		y = rand() % col + 1;//(0~8)+1==1~9
		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}

int AmountMine(char board[ROWS][COLS], int x, int y)
{
	return (board[x - 1][y] +
		board[x - 1][y - 1] +
		board[x][y - 1] +
		board[x + 1][y - 1] +
		board[x + 1][y] +
		board[x + 1][y + 1] +
		board[x][y + 1] +
		board[x - 1][y + 1] - 8 * '0');
}

void FindMine(char show[ROWS][COLS], char mine[ROWS][COLS],int row, int col)
{
	int x = 0, y = 0;
	int win = 0;//统计非雷的个数
	while (win < row*col-AMOUNT)
	{
		printf("请选择排查的坐标(横 纵)\n");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (show[x][y] != '*')
			{
				printf("坐标已被排查，不能重复排查\n");
			}
			else
			{
				if (mine[x][y] == '1')
				{
					printf("你排查到雷了\n");
					Display(mine, ROW, COL);
					break;
				}
				else
				{
					win++;
					int count = AmountMine(mine, x, y);
					show[x][y] = count + '0';
					Display(show, ROW, COL);
				}
			}

		}
		else
		{
			printf("输入错误，请重新输入");
		}
	}
	if (win == row * col - AMOUNT)
	{
		printf("恭喜你，扫雷成功");
		Display(mine, ROW, COL);
	}
}