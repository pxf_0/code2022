#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void game()
{
	//存放雷的棋盘  雷为'1',非雷为'0'  这里是字符1，0
	char mine[ROWS][COLS] = { 0 };
	//展示排查后的棋盘  未排查的区域为'*'，排查后显示周围雷的数字（也是字符）
	char show[ROWS][COLS] = { 0 };
	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');
	SetMine(mine, ROW, COL);
	Display(show, ROW, COL);
	Display(mine, ROW, COL);
	FindMine(show, mine, ROW, COL);
}

int main()
{
	srand((unsigned int)time(NULL));
	int choice = 0;
	do
	{
		menu();
		printf("请选择\n");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:
		{
			game();
			break;
		}
		case 0:
		{
			printf("游戏退出\n");
			break;
		}
		default:
		{
			printf("选择错误，重新选择\n");
			break;
		}
		}
	} while (choice);
	return 0;
}