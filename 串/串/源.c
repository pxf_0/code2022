#define _CRT_SECURE_NO_WARNINGS 1
/*①串的赋值（将一个字符串常量赋值给一个串）；
②求串的实际长度；
③两个串的比较：串1>串1，返回值大于0，串1==串2，返回值等于0，串1<串2，返回值小于0；
④将串清为空串；
⑤两个串的联接；返回长的新串
⑥求子串：返回主串中第i个位置开始的m个字符；
⑦求子串的位置：返回主串中子串第一次出现的位置序号。
⑧删除串中第i个字符开始的j个字符。*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct string
{
	char* ch;
	int length;
}String;

//初始化
void InitString(String* ps)
{
	ps->ch = NULL;
	ps->length = 0;
}

//串的赋值（将一个字符串常量赋值给一个串）；
void CopyString(String* ps, char* str)
{
	if (ps->ch != NULL)
	{
		free(ps->ch);
	}
	int len = strlen(str);
	ps->ch = (char*)malloc(sizeof(char) * (len + 1));
	ps->ch = str;
	ps->ch[len] = '\0';
	ps->length = len;
}

//求长度
int LengthString(String* ps)
{
	return ps->length;
}

//串的比较，串1>串1，返回值大于1，串1==串2，返回值等于0，
//串1<串2，返回值小于-1；

int CompreString(String* str1, String* str2)
{
	if (str1->length == 0 && str2->length == 0)
	{
		return 0;
	}
	int i = 0, j = 0;
	for (i = 0, j = 0; i < str1->length && j < str2->length; i++, j++)
	{
		if (str1->ch[i] == str2->ch[j])
			continue;
		else if (str1->ch[i] > str2->ch[j])
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	if (str1->length > str2->length)
		return 1;
	else if (str1->length < str2->length)
		return -1;
	else
		return 0;
}

//将串清为空串
void ClearString(String* ps)
{
	free(ps->ch);
	ps->length = 0;
}

//两个串的联接；返回长的新串
String* ConnectString(String* ps, String* str1, String* str2)
{
	free(ps->ch);
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	int i = 0, j = 0, k = 0;
	ps->ch = (char*)malloc(sizeof(char) * (len1 + 1 + len2 + 1));

	for (i = 0; i < len1; i++)
	{
		ps->ch[k] = str1->ch[i];
	}
	for (j = 0; j < len2; j++)
	{
		ps->ch[k] = str2->ch[j];
	}
	strcmp(str1->ch, str2->ch);
	ps->length = len1 + len2;
	return ps;
}

//求子串：返回主串中第i个位置开始的m个字符
String* KidString(String* ps, int i, int m)
{
	String* str;
	char arr[50] = { 0 };
	int j = i - 1;
	int k = j + m;
	int t = 0;
	while (j <= m)
	{
		arr[t] = ps->ch[j];
		j++;
	}
	return str;
	//int j = 0;
	//int len = LengthString(ps);
	//for (j=0; j < len-i; j++)
	//{
	//	ps->ch[j] = ps->ch[i];
	//	i++;
	//}
	//return ps;
}

//求子串的位置：返回主串中子串第一次出现的位置序号
int Repostring(String* str1, String* str2,int pos)
{
	int i = pos;
	int j = 0;
	while (i != str1->length && j != str2->length)
	{
		if (str1->ch[i] == str2->ch[j])
		{
			i++;
			j++;
		}
		else
		{
			i = i - j + 1;
			j = 0;
		}
	}
	if (j == str2->length)
		return i - j;
	else
		return -1;
}

//删除串中第i个字符开始的j个字符
void DeleteString(String* ps, int i, int j)
{
	int m = i - 1;
	int n = i + j;
	int pos = n;
	while (pos < ps->length)
	{
		ps->ch[pos] = ps->ch[pos + 1];
		pos++;
	}
}

//打印串
void PrintString(String* ps)
{
	int i = 0;
	int len = LengthString(ps);
	for (i = 0; i < len; i++)
		printf("%c", ps->ch[i]);
}

int main()
{
	String s;
	InitString(&s);
	printf("输入数字查询功能:       1.串的赋值 2.求串的实际长度                             \n");
	printf("3.两个串的比较：串1>串1，返回值大于1，串1==串2，返回值等于0，串1<串2，返回值小于-1\n");
	printf("4.将串清为空串 5.两个串的联接；返回长的新串 6.求子串：返回主串中第i个位置开始的m个字符\n");
	printf("7.求子串的位置：返回主串中子串第一次出现的位置序号 8.删除串中第i个字符开始的j个字符\n");
	printf("\n");
	int n = 0;
	scanf("%d", &n);
	getchar();
	switch (n)
	{
	case 1:
	{
		char arr[50];
		printf("输入字符串\n");
		gets(arr);
		CopyString(&s, &arr);
		PrintString(&s);
		break;
	}
	case 2:
	{
		printf("%d", LengthString(&s));
		break;
	}
	case 3:
	{
		char ch1[50];
		char ch2[50];
		char* ch = ch1;
		printf("请输入第一个字符串\n");
		String str1 = { ch = gets(ch1),strlen(ch) };
		printf("请输入第二个字符串\n");
		String str2 = { ch = gets(ch2),strlen(ch) };
		int tag = CompreString(&str1, &str2);
		if (tag == 1)
		{
			printf("串1>串2\n");
		}
		else if (tag == -1)
		{
			printf("串1<串2\n");
		}
		else
		{
			printf("串1=串2\n");
		}
		break;
	}
	case 4:
	{
		ClearString(&s);
		break;
	}
	case 5:
	{
		char ch1[50];
		char ch2[50];
		char* ch = ch1;
		printf("请输入第一个字符串\n");
		String str1 = { ch = gets(ch1),strlen(ch) };
		printf("请输入第二个字符串\n");
		String str2 = { ch = gets(ch2),strlen(ch) };
		ConnectString(&s, &str1, &str2);
		PrintString(&s);
		break;
	}
	case 6:
	{
		int i = 0, m = 0;
		char ch1[50];
		char* ch = ch1;
		printf("请输入一个字符串\n");
		String str = { ch = gets(ch1),strlen(ch) };
		printf("请输入从第几个字符开始返回？\n");
		scanf("%d", &i);
		printf("请输入返回几个字符\n");
		scanf("%d", &m);
		KidString(&str, i, m);
		PrintString(&str);
		break;
	}
	case 7:
	{
		char str1[50];
		char str2[50];
		int pos = 0;
		printf("请输入主串\n");
		gets(str1);
		printf("请输入子串\n");
		gets(str2);
		Repostring(str1, str2, pos);
		break;
	}
	case 8:
	{
		int i = 0, j = 0;
		printf("请输入字符串\n");
		gets(s);
		printf("请输入第几个字符\n");
		scanf("%d", &i);
		printf("请输入字符后的第几个\n");
		scanf("%d", &j);
		DeleteString(&s, i, j);
		break;
	}
	}
	return 0;
}