#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

#define ROW 3
#define COL 3

//菜单
void Menu();

//初始化棋盘
void InitBoard(char board[ROW][COL],int row,int col);

//棋盘展示
void DisplayBoard(char board[ROW][COL],int row,int col);

//玩家走
void PlayerMove(char board[ROW][COL], int row, int col);

//电脑走
void RobotMove(char board[ROW][COL], int row, int col);

//判断棋盘是否满了
int IsFull(char board[ROW][COL], int row, int col);

//判断输赢
char IsWin(char board[ROW][COL], int row, int col);
