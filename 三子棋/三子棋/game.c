#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void Menu()
{
	printf("*****************\n");
	printf("**1.开始 0.退出**\n");
	printf("*****************\n");
}

void InitBoard(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
			board[i][j] = ' ';
	}
}

void DisplayBoard(char board[ROW][COL], int row, int col)
{
	int i = 0;//i控制行
	for (i = 0; i < row; i++)
	{
		int j = 0;//j控制列
		for (j = 0; j < col; j++)//打印一行的上半部分，用j控制打印完一整行
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)//为了不打印最右边列的分界线
				printf("|");
		}
		printf("\n");//一行的上半部分结束，换行打印下半部分
		if (i < row - 1)//为了不打印最下边的分界线
		{
			int j = 0;
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)//与上半部分对应，不打印最右边的分界线
					printf("|");
			}
			printf("\n");
		}
	}
}

void PlayerMove(char board[ROW][COL], int row, int col)
{
	int x = 0, y = 0;
	while (1)
	{
		scanf("%d %d", &x, &y);
		if (x > row || y > col)
		{
			printf("输入坐标有误，请重新输入\n"); 
		}
		else if (board[x - 1][y - 1] != ' ')
		{
			printf("输入坐标被占用，请重新输入\n");
		}
		else
		{
			board[x - 1][y - 1] = '*';
			break;
		}
	}

}

void RobotMove(char board[ROW][COL], int row, int col)
{
	while (1)
	{
		int x = rand() % row;
		int y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
}

int IsFull(char board[ROW][COL], int row, int col)//满了为1，不满为0
{
	int count = 0;
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] != ' ')
				count++;
		}
	}
	if (count == row * col)
		return 1;
	else
		return 0;
}

char IsWin(char board[ROW][COL], int row, int col)
{
	int i = 0;//控制行
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] !=' ')//行胜利
			return board[i][1];
	}
	int j = 0;//控制列
	for (j = 0; j < col; j++)
	{
		if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j]!=' ')//列胜利
			return board[1][j];
	}
	//右斜胜利
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1]!=' ')
		return board[1][1];
	//左斜胜利
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
		return board[1][1];
	if (IsFull(board, row, col))
		return 'p';
	return 'c';
}