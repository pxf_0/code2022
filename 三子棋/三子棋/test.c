#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void Game()
{
	char board[ROW][COL];//储存数据，建立二维数组
	InitBoard(board, ROW, COL);//初始化棋盘，置为空，否则都是随机值
	DisplayBoard(board,ROW,COL);//展示棋盘
	while (1)
	{
		printf("玩家走，输入相应坐标如：1 1  (行 列)\n");
		PlayerMove(board, ROW, COL);//玩家走
		printf("\n");
		DisplayBoard(board, ROW, COL);//走完展示一下棋盘
		char ch1 = '*';
		char ch2 = '#';
		char ch3 = 'p';
		if (ch1 == IsWin(board, ROW, COL))
		{
			printf("玩家胜利！\n");
			break;
		}
		else if (ch2 == IsWin(board, ROW, COL))
		{
			printf("电脑胜利！\n");
			break;
		}
		else if (ch3 == IsWin(board, ROW, COL))
		{
			printf("平局\n");
			break;
		}
		printf("电脑走\n");
		RobotMove(board, ROW, COL);//电脑走
		printf("\n");
		DisplayBoard(board, ROW, COL);//走完展示一下棋盘
		if (ch1 == IsWin(board, ROW, COL))
		{
			printf("玩家胜利！\n");
			break;
		}
		else if (ch2 == IsWin(board, ROW, COL))
		{
			printf("电脑胜利！\n");
			break;
		}
		else if (ch3 == IsWin(board, ROW, COL))
		{
			printf("平局\n");
			break;
		}
	}
}


int main()
{
	int choice = 0;
	srand ((unsigned int)time(NULL));
	do
	{
		Menu();
		printf("请选择是否开始游戏，1 为开始，0 为退出\n");
		scanf("%d", &choice);
		switch(choice)
		{
		case 1:
		{
			Game();
			break;
		}
		case 0:
			printf("退出游戏\n");
			break;
		}
		
	} while (choice);
	return 0;
}