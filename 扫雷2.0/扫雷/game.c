#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void menu()
{
	printf("************************\n");
	printf("*****    1.play    *****\n");
	printf("*****    0.exit    *****\n");
	printf("************************\n");
}

void InitBoard(char board[ROWS][COLS], int rows, int cols, char ch)
{
	int i = 0, j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = ch;
		}
	}
}

void Display(char board[ROWS][COLS], int row, int col)
{
	printf("-------------------\n");
	int i = 0, j = 0;
	for (j = 0; j <= col; j++)//这个for循环是打印棋盘的列坐标
	{
		printf("%d ", j);
	}
	printf("\n");

	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);//这一行语句是打印棋盘的行坐标
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("-------------------\n");
}

void SetMine(char board[ROWS][COLS], int row, int col)
{
	int x = 0, y = 0;
	int count = AMOUNT;
	while (count)
	{
		x = rand() % row + 1;//(0~8)+1==1~9
		y = rand() % col + 1;//(0~8)+1==1~9
		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}

int AmountMine(char board[ROWS][COLS], int x, int y)
{
	return (board[x - 1][y] +
		board[x - 1][y - 1] +
		board[x][y - 1] +
		board[x + 1][y - 1] +
		board[x + 1][y] +
		board[x + 1][y + 1] +
		board[x][y + 1] +
		board[x - 1][y + 1] - 8 * '0');
}

void FlagMine(char show[ROWS][COLS], char mine[ROWS][COLS], int row, int col, int x, int y)
{
	if (x >= 1 && x <= row && y >= 1 && y <= col)
	{
		show[x][y] = '!';
	}
}

void FlagCancel(char show[ROWS][COLS], char mine[ROWS][COLS], int row, int col, int x, int y)
{
	if (show[x][y] == '!')
	{
		show[x][y] = '*';
	}
}


void Spread(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	int around_x = 0;
	int around_y = 0;
	int count = 0;
	//坐标合法
	if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
	{
		//遍历周围坐标
		for (around_x = -1; around_x <= 1; around_x++)
		{
			for (around_y = -1; around_y <= 1; around_y++)
			{
				//如果这个坐标不是雷
				if (mine[x + around_x][y + around_y] == '0')
				{
					//统计周围雷的个数
					count = AmountMine(mine, x + around_x, y + around_y);
					if (count == 0)
					{
						if (show[x + around_x][y + around_y] == '*')
						{
							show[x + around_x][y + around_y] = ' ';
							Spread(mine, show, x + around_x, y + around_y);
						}
					}
					else
					{
						show[x + around_x][y + around_y] = count + '0';
					}
				}
			}
		}
	}
}

int ClearMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int i = 0, j = 0;
	int end = 0;
	for (i = 1; i <= row; i++)
	{
		for (j = 1; j <= col; j++)
		{
			if (mine[i][j] == '1' && show[i][j] == '!')
				end++;
		}
	}
	return end;
}

void FindMine(char show[ROWS][COLS], char mine[ROWS][COLS], int row, int col)
{
	int n = 0, x = 0, y = 0;
	int win = 0;
	int flag = 0;
	int fail = 0;
again:
	while (win < AMOUNT)
	{
		printf("功能 1：排查雷 2：标记雷 3：取消标记\n");
		printf("请选择功能和排查的坐标(功能 横 纵)\n");
		scanf("%d %d %d", &n, &x, &y);
		switch (n)
		{
		case 1:
			break;
		case 2:
		{
			FlagMine(show, mine, row, col, x, y);
			win++;
		    break; 
		}
		case 3:
		{
			FlagCancel(show, mine, row, col, x, y); 
			flag = 1;
			win--;
			break;
		}
		}
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (mine[x][y] == '1' && show[x][y] == '*' && flag == 0)
			{
				printf("你被炸死了，下面是雷区分布图 1为雷  0非雷\n");
				Display(mine, ROW, COL);
				fail = 1;
				break;
			}
			else if (show[x][y] == '!')
				Display(show, ROW, COL);
			else if (flag == 1)
				Display(show, ROW, COL);
			else 
			{
				int count = AmountMine(mine, x, y);
				if (count == 0)
				{
					show[x][y] = ' ';
					Spread(mine, show, x, y);
					Display(show, ROW, COL);
				}
				else
				{
					show[x][y] = count + '0';
					Display(show, ROW, COL);
				}
			}
		}
		else
		{
			printf("输入错误，请重新输入");
		}
	}
	if (AMOUNT == ClearMine(mine,show,row,col))
	{
		printf("恭喜你，扫雷成功\n");
	}
	else if(fail == 1)
	{
		printf("请选择是否再来一把\n");
	}
	else
	{
		printf("标记的雷有误，请仔细检查，重新标记\n");
		goto again;
	}
}