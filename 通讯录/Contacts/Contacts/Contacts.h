#pragma once

#include<stdio.h>
#include<string.h>
#include<assert.h>

#define MAX 100

//人的类型
typedef struct Peo
{
	char name[20];
	char sex[5];
	int age;
	char tele[12];
	char addr[30];
}peo;

//通讯录
typedef struct Con
{
	peo data[MAX];
	int count;
}Contact;


//初始化通讯录
void InitCont(Contact* pc);

//增加个人信息
void AddInfo(Contact* pc);

//显示全部信息
void ShowInfo(Contact* pc);

//查找指定的人
void Find_by_name(Contact* pc);

//删除指定的人
void Dele_by_name(Contact* pc);

//修改指定的人
void Modi_by_name(Contact* pc);

//删除通讯录
void ClearInfo(Contact* pc);

//按照名字排序
void Sort_by_name(Contact* pc);