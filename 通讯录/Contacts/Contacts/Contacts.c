#define _CRT_SECURE_NO_WARNINGS 1

#include"Contacts.h"

void InitCont(Contact* pc)
{
	assert(pc);
	pc->count = 0;
	memset(pc->data, 0, sizeof(pc->data));
}

void AddInfo(Contact* pc)
{
	assert(pc);

	if (pc->count == MAX)
	{
		printf("通讯录已满！\n");
		return;
	}
	else
	{
		printf("请输入姓名：\n");
		scanf("%s", pc->data[pc->count].name);//数组名就是地址
		printf("请输入性别：\n");
		scanf("%s", pc->data[pc->count].sex);
		printf("请输入年龄：\n");
		scanf("%d", &(pc->data[pc->count].age));
		printf("请输入电话：\n");
		scanf("%s", pc->data[pc->count].tele);
		printf("请输入地址：\n");
		scanf("%s", pc->data[pc->count].addr);

		pc->count++;

		printf("添加成功\n");
	}
}

void ShowInfo(Contact* pc)
{
	assert(pc);
	
	printf("%-10s\t%-4s\t%-4s\t%-12s\t%-20s\n", "姓名","性别", "年龄", "电话", "地址");
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		printf("%-10s\t%-4s\t%-4d\t%-12s\t%-20s\n",    pc->data[i].name,
													   pc->data[i].sex,
													   pc->data[i].age,
													   pc->data[i].tele,
													   pc->data[i].addr);
	}

}

void Find_by_name(Contact* pc)
{
	assert(pc);
	char name[20];
	if (pc->count>0)
	{
		printf("请输入查找人的姓名:\n");
		scanf("%s", name);
		int i = 0;
		for (i = 0; i < pc->count; i++)
		{
			if (strcmp(pc->data[i].name, name) == 0)
			{
				printf("%-10s\t%-4s\t%-4s\t%-12s\t%-20s\n", "姓名", "性别", "年龄", "电话", "地址");
				printf("%-10s\t%-4s\t%-4d\t%-12s\t%-20s\n", pc->data[i].name,
															pc->data[i].sex,
															pc->data[i].age,
															pc->data[i].tele,
															pc->data[i].addr);
				return;
			}
			else
			{
				printf("找不到此人\n");
				return;
			}
		}
	}
	else
	{
		printf("通讯录为空\n");
	}
}

int _find(Contact* pc,char* name)
{
	assert(pc && name);
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
		{
			return i;
		}
		else
			return -1;
	}
}

void Dele_by_name(Contact* pc)
{
	assert(pc);
	char name[20];
	if (pc->count > 0)
	{
		printf("请输入要删除人的姓名：\n");
		scanf("%s", name);
		int pos = _find(pc,name);
		if (pos == -1)
		{
			printf("没有此人，删除失败\n");
		}
		else
		{
			for (int i = pos; i < pc->count - 1; i++)
			{
				pc->data[i] = pc->data[i + 1];
			}

			pc->count--;
			printf("删除成功\n");
		}
	}
	else
	{
		printf("通讯录为空，无法删除\n");
	}
}

void Modi_by_name(Contact* pc)
{
	char name[20];
	printf("请输入修改人的姓名：\n");
	scanf("%s", name);
	if (pc->count > 0)
	{
		int pos = _find(pc, name);
		if (pos == -1)
		{
			printf("未找到此人\n");
			return;
		}
		else
		{
			printf("以下输入为新信息\n");
			printf("请输入姓名：\n");
			scanf("%s", pc->data[pos].name);
			printf("请输入性别：\n");
			scanf("%s", pc->data[pos].sex);
			printf("请输入年龄：\n");
			scanf("%d", &(pc->data[pos].age));
			printf("请输入电话：\n");
			scanf("%s", pc->data[pos].tele);
			printf("请输入地址：\n");
			scanf("%s", pc->data[pos].addr);
		}
	}
	else
	{
		printf("通讯录为空\n");
	}
}

void ClearInfo(Contact* pc)
{
	assert(pc);

	pc->count = -1;
}


int _sort_by_name(void* e1, void* e2)
{
	return (strcmp((peo*)e1, (peo*)e2));
}

void Sort_by_name(Contact* pc)
{
	assert(pc);

	if (pc->count > 0)
	{
		qsort(pc->data, pc->count, sizeof(peo), _sort_by_name);
		printf("排序成功\n");
	}
	else
	{
		printf("通讯录为空\n");
	}
}