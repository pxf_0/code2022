#define _CRT_SECURE_NO_WARNINGS 1
#include"Contacts.h"

void menu()
{
	printf("****************************\n");
	printf("****1.add      2.del  ******\n");
	printf("****3.find     4.mod  ******\n");
	printf("****5.show     6.clear******\n");
	printf("****7.sortbyname      ******\n");
	printf("****0.exit            ******\n");
	printf("****************************\n");
}

int main()
{
	Contact con;
	InitCont(&con);
	int choice = 0;
	do
	{
		menu();
		printf("请选择:\n");
	    scanf("%d", &choice);
		switch (choice)
		{
		case 1:

			AddInfo(&con);
			break;
		case 2:
			Dele_by_name(&con);
			break;
		case 3:
			Find_by_name(&con);
			break;
		case 4:
			Modi_by_name(&con);
			break;
		case 5:
			ShowInfo(&con);
			break;
		case 6:
			ClearInfo(&con);
			break;
		case 7:
			Sort_by_name(&con);
			break;
		case 0:
			break;
		default:
			printf("输入错入，请重新输入");
			scanf("%d", &choice);
			break;
		}
	} while (choice);

	return 0;
}